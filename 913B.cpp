#include <iostream>
#include <vector>
#include <string>
#include <cstring>

using namespace std;

int n;
vector<int> c[1001];

int main(){
    cin>>n;
    int no;
    for(int i=1;i<n;++i){
        cin>>no;
        c[no].push_back(i+1);
    }

    string ans="Yes";

    for(int i=1;i<=n;++i){
        if(c[i].size()>0){
            int nol=0;
            for(int ch:c[i]){
                if(c[ch].size()==0)++nol;
            }
            if(nol<3){ans="No";break;}
        }
    }


    cout<<ans<<endl;

}
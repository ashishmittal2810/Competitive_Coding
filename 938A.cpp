#include <iostream>
#include <string>

using namespace std;

int n;
string s;

int stateC(char c){
    if(c=='a' || c=='e' || c=='i' || c=='o' || c=='u' || c=='y')return 1;
    else return 0;
}

int main(){
    cin>>n;
    cin>>s;

    int state=0;
    string ans="";
    for(int i=0;i<n;++i){
        if(!state || !stateC(s[i])){
            ans+=s[i];state=stateC(s[i]);
        }
    }
    cout<<ans<<endl;
}
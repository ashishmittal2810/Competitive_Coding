#include <deque>
using namespace std;

void getMinInWindowSizeK(int* src,int k,int siz,int* dest){
    deque<int> mins;
    mins.push_back(siz-1);
    for(int i=siz-2;i>siz-k;--i){
        while(!mins.empty() && src[mins.back()]>src[i])mins.pop_back();
        mins.push_back(i);
    }
    for(int i=siz-k;i>=0;--i){
        while(!mins.empty() && src[mins.back()]>src[i])mins.pop_back();
        mins.push_back(i);
        while(!mins.empty() && mins.front()>=i+k)mins.pop_front();
        dest[i]=src[mins.front()];
    }
}
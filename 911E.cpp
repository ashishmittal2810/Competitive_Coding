#include <iostream>
#include <vector>

#define PII pair<int,int>

using namespace std;

static const int maxN=2e+5;


int n,k;
int nos[maxN]={};
int printed[maxN]={};

PII nextSeq(int ind){
	int no=nos[ind];
	bool decSt=true;
	int ma=no;
	int found=1;
	int in=ind+1;
	
	while(in<k and decSt){
		if(nos[in]<nos[in-1]){in++;found++;}
		else decSt=false;
	}

	while(in<k){
		if(nos[in]>nos[in-1]){ma=max(ma,nos[in]);in++;found++;}
		else break;
	}

	return PII(found,ma);

}

bool verifyCorrectness(int start,int ma){
	for(int i=start;i<k;++i){
		printed[nos[i]]=1;
	}

	if(start==(k-1))return true;
	else if(nos[k-1]<nos[k-2])return true;
	else{
		for(int i=start+1;i<=nos[k-1];++i){
			if(!printed[i])return false;
		}
		return true;
	}
}

void completeSequence(int start,int ma){
	for(int i=nos[k-1];i>start;--i){
		if(!printed[i])cout<<i<<" " ;
	}
	for(int i=nos[k-1];i<ma;++i){
		if(!printed[i])cout<<i<<" " ;
	}
}

int main(){
	cin>>n>>k;
	for(int i=0;i<k;++i){
		cin>>nos[i];
	}

	int index=0;
	PII p;
	while(true){
		p=nextSeq(index);
		index+=p.first; 	
		if(index==k){
			bool poss=verifyCorrectness(index-p.first,p.second);
			if(!poss){cout<<"-1"<<endl;return 0;}
			else{
				for(int i=0;i<k;++i)cout<<nos[i]<<" ";
				completeSequence(index-p.first,p.second);
				for(int i=n;i>p.second;--i)cout<<i<<" ";
				cout<<endl;
			}
			return 0;
		}
		else if(index!=p.second){cout<<"-1"<<endl;return 0;}
	}

}
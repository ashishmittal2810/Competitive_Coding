#include <iostream>

#define ll long long

using namespace std;

ll n,k,a,b;

int main(){
    cin>>n>>k>>b>>a;
    ll ans=0;
    if(k==1){ans=(n-1)*b;}
    else while(n>1){
        if(n%k){ans+=n%k*b;n=n/k*k;if(!n)ans-=b;}
        else{
            ll newN=n/k;
            if(a>(n-newN)*b)ans+=(n-newN)*b;
            else ans+=a;
            n=newN;
        }
    }
    cout<<ans<<endl;
}
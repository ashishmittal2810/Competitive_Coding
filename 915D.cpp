#include <iostream>
#include <vector>
#include <deque>

#define REP(i,n) for(int i=0;i<n;++i)
#define P pair<int,int>
#define to first
#define in second

using namespace std;

vector<P> conn[500];
deque<P*> s;
int inDegree[500];
vector<bool> visited(500,false);
vector<bool> visitc(500,false);
int n,m;

int cycle=0;

void dfs(int x){
    if(visited[x]){
        cycle++;
        //cout<<"com "<<x<<" "<<s.size()<<endl;
        auto it=s.begin();
        (*it)->in++;
        //cout<<"it "<<x<<" "<<(*it)->to<<" "<<(*it)->in<<endl;
        ++it;
        for(;it!=s.end() && (*it)->to!=x;++it){
            (*it)->in++;
            //cout<<"it "<<x<<" "<<(*it)->to<<" "<<(*it)->in<<endl;
        }
    }
    else{
        visited[x]=true;
        for(auto it=conn[x].begin();it!=conn[x].end();++it){
            if(!visitc[it->to]){
                //cout<<"pushBef "<<s.size()<<endl;
                s.push_front(&(*it));
                //cout<<"pushing "<<s.size()<<" "<<x<<" "<<it->first<<endl;;
                dfs(it->to);
            }
        }
        //cout<<"removing "<<s.size()<<endl;
        visited[x]=false;
        visitc[x]=true;
    }
    if(!s.empty())s.pop_front();
}

int main(){
    cin>>n>>m;
    int u,v;
    
    REP(i,m){
        cin>>u>>v;
        u--;v--;
        conn[u].push_back(P(v,0));
        inDegree[v]++;
    }

    REP(i,n)if(!visitc[i])dfs(i);

    bool ans=false;
    REP(i,n){
        for(auto it=conn[i].begin();it!=conn[i].end();++it){
            cout<<i<<" "<<it->in<<" "<<it->in<<" "<<cycle<<endl;
            if(it->in==cycle){cout<<"YES"<<endl;return 0;}
        }
    }
    cout<<"NO"<<endl;

}

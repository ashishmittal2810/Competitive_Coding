#include <iostream>
#include <string>
#include <vector>

using namespace std;

const int maxN=1e+6;

char c[maxN];
int n;
int k;

int main(){
    cin>>n;
    for(int i=0;i<n;++i)cin>>c[i];
    cin>>k;
    int cntp=0;
    for(int i=0;i<n;++i){
        if(c[i]=='F'){
            if(cntp>=k)for(int j=i-cntp;j<i;++j)c[j]='F';
            cntp=0;
        }
        else cntp++;
    }
    if(cntp>=k)for(int j=n-cntp;j<n;++j)c[j]='F';

    int cntf=0;
    bool poss=true;

    for(int i=0;i<n && poss;++i){
        if(c[i]=='F')cntf++;
        else {
            if(cntf>0 && cntf<k)poss=false;
            cntf=0;
        }
    }
    if(cntf>0 && cntf<k)poss=false;
    if(poss)cout<<"YES"<<endl;
    else cout<<"NO"<<endl;
}
#include <iostream>

#define ll long long
#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

ll n,k;

ll l1=1LL;
int no[64]={};
int ans[128]={};

int main(){
    cin>>n>>k;

    int ones=0;

    REP(i,64){
        if(n&(l1<<i)){no[i]=1;ans[i+64]=1;ones++;}
    }

    //cout<<ones<<" "<<k<<endl;
    if(ones>k){cout<<"No"<<endl;return 0;}

    int ind=127;
    bool dc=false;
    bool spec=false;
    int ik;
    while(ones!=k && ind!=0){
        //cout<<ind<<" "<<ones<<endl;
        if(ans[ind]){
            if((k-ones)>=ans[ind]){ans[ind]--;ans[ind-1]+=2;ones++;}
            else {
                int in=-1;
                for(int j=ind-1;j>=0;--j){
                    if(ans[j])in=j;
                }
                //cout<<ind<<" "<<in<<" "<<ans[ind]<<" "<<ans[in]<<" "<<ones<<" "<<k<<" "<<(k-ones)<<endl;
                if(in==-1){ans[ind]--;ans[ind-1]+=2;ones++;}
                else {ans[in]--;ik=in;spec=true;break;};
            }

        }
        else ind--;
    }

    if(ones!=k && !spec)cout<<"No"<<endl;
    else {
        cout<<"Yes"<<endl;
        // for(int i=0;i<64;++i)if(no[i])cout<<i<<endl;
        for(int i=127;i>=0;--i){
            if(ans[i]){
                REP(j,ans[i])cout<<(i-64)<<" ";
            }
        }
        if(spec){
            while(ones!=k){
                ik--;cout<<(ik-64)<<" ";ones++;
            }
            cout<<(ik-64);
        }
        cout<<endl;
    }

}
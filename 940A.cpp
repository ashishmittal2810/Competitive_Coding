#include <iostream>
#include <algorithm>

using namespace std;


int a[100];
int n,d;

int main(){
    cin>>n>>d;
    for(int i=0;i<n;++i)cin>>a[i];
    sort(a,a+n);
    int ans=1000;
    for(int i=0;i<n;++i){
        for(int j=n-1;j>=i;--j){
            if(a[j]-a[i]<=d){
                ans=min(ans,i+n-1-j);
                break;
            }
        }
    }
    cout<<ans<<endl;
}
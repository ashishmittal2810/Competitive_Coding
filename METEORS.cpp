#include <iostream>
#include <vector>
#include <algorithm>

#define REP(i,n) for(int i=0;i<n;++i)
#define FOR(i,j,n) for(int i=j;i<n;++i)


using namespace std;

const int maxN=(3e+5)+1; 

struct Query{
    int l,r,a;
    Query(int _l=-1,int _r=-1,int _a=-1){l=_l;r=_r;a=_a;}
};

vector<int> meteors[maxN];
int reqMeteros[maxN];
vector<int> QMem[maxN];
Query queries[maxN];
int n,m,k;
int L[maxN],R[maxN];//,Q[maxN];
vector<bool> doneFor;
int doneForC=0;
int meteorCnts[maxN];
int ans[maxN];

void resetMtrCnts(){fill(meteorCnts,meteorCnts+m,0);}

int updateMtrCnts(Query q,int *v=NULL,int i=0,int l=0,int r=m-1){
    //if(i==0 && v==NULL)cout<<q.l<<" "<<q.r<<" "<<q.a<<" "<<i<<" "<<l<<" "<<r<<endl;
    if(l>=q.l && r<=q.r)meteorCnts[i]+=((r-l+1)*q.a);
    else{
        int m=(l+r)/2;
        int f=2*i+1;
        int s=2*i+2;
        if((meteorCnts[f]+meteorCnts[s])<meteorCnts[i]){
            int diff=(meteorCnts[i]-(meteorCnts[f]+meteorCnts[s]))/(r-l+1);
            meteorCnts[f]+=((m-l+1)*diff);
            meteorCnts[s]=((r-m)*diff);
        }
        if(q.l<=m)updateMtrCnts(q,v,f,l,m);
        if(q.r>m)updateMtrCnts(q,v,s,m+1,r);
    }
    if(v!=NULL && q.l==l && q.r==r)*v=meteorCnts[i];
}

int getVal(int x){
    int r;
    updateMtrCnts(Query(x,x,0),&r);
    return r;
}

int main(){
    cin>>n>>m;
    int mem;
    REP(i,m){cin>>mem;meteors[mem-1].push_back(i);}
    REP(i,n)cin>>reqMeteros[i];
    cin>>k;
    int l,r,a;
    REP(i,k){cin>>l>>r>>a;queries[i]=Query(l-1,r-1,a);}

    REP(i,n)R[i]=k-1;
    doneFor=vector<bool>(n,false);
    fill(ans,ans+n,-1);
    while(doneForC!=n){
        REP(i,n){
            if(L[i]!=R[i])QMem[(L[i]+R[i])/2].push_back(i);
            else if(!doneFor[i]){QMem[(L[i]+R[i])/2].push_back(i);doneFor[i]=true;doneForC++;}
        }
        REP(i,k){
            Query q=queries[i];
            if(q.l<=q.r)updateMtrCnts(q);
            else{
                updateMtrCnts(Query(q.l,m-1,q.a));
                updateMtrCnts(Query(0,q.r,q.a));
            }
            for(int mem:QMem[i]){
                int tot=0;
                for(int met:meteors[mem])tot+=getVal(met);
                //cout<<"stats "<<i<<" "<<mem<<" "<<tot<<" "<<reqMeteros[mem]<<endl;
                if(tot>=reqMeteros[mem]){R[mem]=i;ans[mem]=i;}
                else L[mem]=i+1;
            }
        }
        resetMtrCnts();
        REP(i,n)QMem[i].clear();
    }

    REP(i,n)if(ans[i]+1)cout<<(ans[i]+1)<<endl;else cout<<"NIE"<<endl;

}
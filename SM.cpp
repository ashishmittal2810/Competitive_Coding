#include <iostream>
#include <map>
#include <vector>

#define REP(i,n) for(int i=0;i<n;++i)
#define FOR(i,j,n) for(int i=j;i<n;++i)

using namespace std;

const int maxN=1e+5;

map<int,int> cnt;
map<int,int> cost;

int b[maxN+1];
vector<int> conn[maxN+1];

int n;

void dfs(int nod,int p,int h){
    for(int a:conn[nod]){
        if(a!=p){
            dfs(a,nod,h+1);

        }
    }

}

int main(){
    cin>>n;
    REP(i,n)cin>>b[i];
    int u,v;
    REP(i,n-1){
        cin>>u>>v;
        conn[u].push_back(v);
        conn[v].push_back(u);
    }

    dfs(1,-1,0);

}


#include <iostream>
#include <queue>
#include <functional>
#include <vector>

#define ll long long
#define REP(i,n) for(int i=0;i<n;++i)
#define P pair<ll,int>

using namespace std;

const ll inf=2e+18;

int n,k;
ll p[16];

priority_queue<P,vector<P>,greater<P> > q;

int main(){
    cin>>n;
    REP(i,n)cin>>p[i];
    cin>>k;

    if(k==1)cout<<1<<endl;
    else{
        q.push(P(p[0],0));

        P pa;
        int t;
        ll no;
        ll temp;
        for(int i=0;i<k;++i){
            pa=q.top();
            q.pop();
            //cout<<(i+1)<<" "<<pa.first<<" "<<pa.second<<endl;
            if(i==(k-2))break;
            ll no=pa.first;
            int t=pa.second;
            if(t<(n-1)){
                temp=no/p[t];
                if(p[t+1]<(inf/temp))q.push(P((no/p[t])*p[t+1],t+1));
            }
            if(t<n){
                if(p[t]<(inf/no))q.push(P(no*p[t],t));
            }
        }
        cout<<pa.first<<endl;
    }
}
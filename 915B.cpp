#include <iostream>
#include <algorithm>

#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

int n,p,l,r;

int main(){
    cin>>n>>p>>l>>r;
    int ans=0;
    if(p>r){
        ans=(p-r)+1;
        if(l>1)ans+=((r-l)+1);
    }
    else if(p<l){
        ans=(l-p)+1;
        if(n>r)ans+=((r-l)+1);
    }
    else{
        int a=r-p;
        int b=p-l;
        if(r<n)ans+=(a+1);
        if(l>1)ans+=(b+1);
        if(ans==((r-l)+2))ans+=min(a,b);
    }
    cout<<ans<<endl;
}
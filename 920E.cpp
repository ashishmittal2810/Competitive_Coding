#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

#define REP(i,n) for(int i=0;i<n;++i)
#define FOR(i,j,n) for(int i=j;i<n;++i)

using namespace std;

const int maxN=(2e+5)+5;

set<int> s;

//For each node keep a visit boolean.Iterate and if it is false make it true and then take the first element on sorted list of element to wwhich it has edge(not in real graph).
//iterate till the iterator with for e ach elemnt removeing all eles from this list for which this ele has an edge.When we reac the iterator's value if it is still in ist incremnt by
//2 to skip it and else not.In both case incremnt the iterator.
int ans=0;
int n,m;
int visited[maxN]={};
set<int> conn[maxN];
vector<int> sizs;
vector<int> lft;

void removeAll(int node,int f){
    visited[node]=1;
    auto itf=conn[f].begin();
    auto itn=conn[node].begin();
    while(itf!=conn[f].end() && itn!=conn[node].end()){
        if(*itn<*itf)++itn;
        else{
            if(*itf<*itn){lft.push_back(*itf);conn[f].erase(itf);++itf;}
            else{itf++;itn++;}
        }
    }
}

int main(){
    cin>>n>>m;
    int a,b;
    
    REP(i,m){
        cin>>a>>b;
        a--;b--;
        conn[a].insert(b);
        conn[b].insert(a);
    }

    REP(i,n){
        if(!visited[i]){
            //cout<<i<<" "<<conn[i].size()<<endl;
            visited[i]=1;
            ans++;
            int j=i+1;
            auto it=conn[i].begin();
            while(it!=conn[i].end() && *it<j)++it;
            while(j<n){
                //todo:keep track of lost values
                int till=(it==conn[i].end())?n:*it;
                if(j==till)++it;
                else removeAll(j,i);
                j++;
            }
            //cout<<"siz "<<lft.size()<<endl;
            while(!lft.empty()){
                int siz=lft.size();
                REP(l,siz)removeAll(lft[l],i);
                lft.erase(lft.begin(),lft.begin()+siz);
            }
            sizs.push_back(n-conn[i].size());
            //cout<<"visit "<<i<<" "<<sizs[sizs.size()-1]<<endl;
        }
    }

    cout<<ans<<endl;
    sort(sizs.begin(),sizs.end());
    REP(i,ans)cout<<sizs[i]<<" ";
    cout<<endl;

}
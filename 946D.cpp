#include <iostream>
#include <vector>
#include <algorithm>

#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

const int _=505;
const int inf=1e+9;

int dpt[_][_]={};
int dpd[_][_]={};
vector<int> cls[_]={};

int n,m,k;

int solveT(int q,int e){
    //cout<<"solveT "<<q<<" "<<e<<endl;
    int& s=dpt[q][e];
    if(s==-1){
        int siz=cls[q].size();
        if(e==siz)s=0;
        else {
            int dif=cls[q][siz-1]-cls[q][0]+1;
            s=dif;
            REP(i,e+1)s=min(s,dif-(cls[q][i]-cls[q][0]+cls[q][siz-1]-cls[q][siz-1-(e-i)]));
        }
    }
    //cout<<q<<" "<<e<<" "<<s<<endl;
    return s;
}

int solveD(int q,int e){
    //cout<<"solveD "<<q<<" "<<e<<endl;
    if(q>=n)return 0;
    //if(e==0)return 0;
    int&s=dpd[q][e];
    if(s==-1){
        s=solveT(q,0)+solveD(q+1,e-0);
        int till=min(e,(int)cls[q].size())+1;
        //cout<<"till "<<till<<endl;
        REP(i,till)s=min<int>(s,solveT(q,i)+solveD(q+1,e-i));
        //cout<<"sol "<<q<<" "<<e<<" "<<s<<endl;
    }
    return s;
}

int main(){
    cin>>n>>m>>k;
    char inp;
    REP(i,n)REP(j,m){cin>>inp;if(inp-'0')cls[i].push_back(j);dpt[i][j]=-1;}
    REP(i,n)REP(j,k+1)dpd[i][j]=-1;
    cout<<solveD(0,k)<<endl;
}
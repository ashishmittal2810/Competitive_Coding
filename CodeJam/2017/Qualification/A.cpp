#include <iostream>
#include <cstring>

using namespace std;
 
int t;

int main(){
    cin>>t;
    int no=1;
    while(t--){
        string s;
        int l,k;
        cin>>s;
        cin>>k;
        l=s.length();
        bool poss=true;
        int ans=0;
        for(int i=0;i<l;++i){
            if(s[i]=='-'){
                if(i<=(l-k)){
                    ans++;
                    for(int j=0;j<k;++j)
                        if(s[i+j]=='+')s[i+j]='-';
                        else s[i+j]='+';
                }
                else {poss=false;break;}
            }
        }
        cout<<"Case #"<<no<<": ";
        if(poss)cout<<ans<<endl;
        else cout<<"IMPOSSIBLE"<<endl;
        no++;
    }
}
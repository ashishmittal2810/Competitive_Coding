#include <iostream>
#include <cstring>

using namespace std;

int t;
string s,ans;

void backtrack(int i){
    while(ans[i]=='0'){ans[i]='9';--i;}
    ans[i]--;
    while(i>0 && ans[i]<ans[i-1]){
        ans[i]='9';
        ans[--i]--;
    }
}

int main(){
    cin>>t;
    int no=1;
    while(t--){
        cin>>s;
        ans="";
        int len=s.length();
        char prev='0';
        bool all9=false;
        for(int i=0;i<len;++i){
            if(all9)ans+='9';
            else{
                if(s[i]>=prev){ans+=s[i];prev=s[i];}
                else{backtrack(i-1);ans+='9';all9=true;}
            }
        }
        if(ans[0]=='0')ans=ans.substr(1,ans.length()-1);
        cout<<"Case #"<<no<<": "<<ans<<endl;
        no++;
    }
}

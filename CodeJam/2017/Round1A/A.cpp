#include <iostream>

#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

string rows[25]={};
int t;
int r,c;

void back(int a,int b,char ch){
    while(b>=0 && rows[a][b]=='?'){rows[a][b]=ch;--b;}
}

void up(int a,int b,char ch){
    while(a>=0 && rows[a][b]=='?'){rows[a][b]=ch;--a;}
}

int main(){
    cin>>t;
    int no=1;
    while(t--){
        cin>>r>>c;
        REP(i,r) cin>>rows[i];
        int lastU=-1;
        REP(i,r){
            char prev='.';
            REP(j,c)if(rows[i][j]!='?'){back(i,j-1,rows[i][j]);prev=rows[i][j];}
            if(prev!='.'){
                back(i,c-1,prev);
                REP(j,c)up(i-1,j,rows[i][j]);
                lastU=i;
            }
        }
        if(lastU!=-1)REP(i,c)up(r-1,i,rows[lastU][i]);
        cout<<"Case #"<<no<<":"<<endl;
        REP(i,r) cout<<rows[i]<<endl;
        no++;
    }
}
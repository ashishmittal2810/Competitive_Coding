#include <isotream>
#include <queue>
#include <map>
#include <algorithm>
#include <functional>
#include <vector>

#define P pair<double,int>
#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

int t;
int n,k;
double u;
priority_queue<P,vector<P>,greater<P>> q;
double nos[50];

int main(){
    cin>>t;
    int no=0;
    while(t--){
        map<double,int> m;
        double ans=1;
        
        cin>>n>>k>>u;
        
        REP(i,n)cin>>nos[i];
        sort(nos,nos+n);
        
        REP(i,n-k)ans*=nos[i];
        
        for(int i=n-k;i<n;++i)m[nos[i]]++;
        for(auto it:m)q.push(P(it->first,it->second));

        P p,p_;
        while(u!=0){
            p=q.front();
            q.pop();
            p_=q.front();
            float ti=min((p_.first-p.first)*p.second,u);
            if(ti==u){
                p.first=min(1,p.first+(u/p.second));
                q.push(p);
            }
            else{
                p_.second+=p.second;
                q.pop();
                q.push(p_);
            }
        }
        

        cout<<"Case #"<<no<<":";
        cout<<ans<<endl;
        no++;
    }
}
#include <iostream>
#include <algorithm>

#define REP(i,n) for(int i=0;i<n;++i)
#define ll long long

using namespace std;
const int maxN=700;

ll a[maxN][maxN]={};


int t;
int n;
int main(){
    cin>>t;
    while(t--){
        cin>>n;
        REP(i,n)REP(j,n)cin>>a[i][j];
        REP(i,n)sort(a[i],a[i]+n);
        ll ans=a[n-1][n-1];
        ll l=a[n-1][n-1];
        bool all=true;
        for(int i=n-2;i>=0 && all;--i){
            int j=n-1;
            while(j>=0 && a[i][j]>=l)--j;
            if(j>=0){ans+=a[i][j];l=a[i][j];}
            else all=false;
        }   
        if(all)cout<<ans<<endl;
        else cout<<-1<<endl;
    }
}
#include <iostream>
#include <cstring>


using namespace std;

string s;

int main(){
    cin>>s;
    int n=s.length();

    int state=0;
    for(int i=0;i<n;++i){
        if(state==26)break;
        else if(s[i]-'a'<=state)s[i]='a'+state++;

    }
    if(state==26)cout<<s<<endl;
    else cout<<-1<<endl;
}
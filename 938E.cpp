#include <iostream>
#include <algorithm>

#define ll long long

using namespace std;


const int M=(1e+9)+7;
const int maxn=1e+6;

ll mod(ll no,int d=M-2){
    if(d==1)return no;
    if(d%2)return (no*mod(no,d-1))%M;
    else{
        ll h=mod(no,d/2);
        return (h*h)%M;
    }
}

int n;
ll nos[maxn];

int main(){
    cin>>n;
    for(int i=0;i<n;++i)cin>>nos[i];
    
    sort(nos,nos+n);
    
    ll facModP=1;
    ll facMod=1;
    for(int i=2;i<n;++i)facModP=(facModP*i)%M;
    facMod=(facModP*n)%M;

    int ans=0;
    int prev=-1;
    int j;
    for(int i=0;i<n;++i){
        if(prev<nos[i])j=i;
        ans=(ans+(((nos[i]*facMod)%M)*mod(n-j))%M)%M;
        ans=(ans+M-(nos[i]*facModP)%M)%M;
        prev=nos[i];
    }
    cout<<ans<<endl;

}
#include <iostream>
#include <algorithm>

#define ll long long

using namespace std;

int n,l;
int c[30];

int main(){
    cin>>n>>l;
    for(int i=0;i<n;++i)cin>>c[i];
    ll ans=0;
    int till=n-1;
    while(l){
        float eff=2e+9;
        int a=-1;
        float e;
        for(int i=0;i<=till;++i){
            e=(float)c[i]/(float)(min<float>(l,1<<i));
            if(e<eff){
                eff=e;a=i;
            }
        }
        int d=l/min<int>(l,(1<<a));
        ans+=((ll)d*(ll)c[a]);
        l-=(d*min<int>(l,1<<a));
        till=a;
    }
    cout<<ans<<endl;
}
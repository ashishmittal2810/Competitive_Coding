#include <iostream>
#include <algorithm>

using namespace std;

struct Triple{
    int t,c,i;
    Triple(int _t=-1,int _c=-1,int _i=-1){
        t=_t;c=_c;i=_i;
    }
};

const int maxN=(2e+5)+1;

int n,T;
Triple v[maxN];

bool verify(int a,bool print=false){
    int t=0;
    int d=0;
    for(int i=0;d<a && i<n;++i){
        if(v[i].t>=a){
            d++;
            t+=v[i].c;
            if(print)cout<<v[i].i<<" ";
        }
    }
    if(print)cout<<endl;
    if(t<=T)return true;
    else return false;
}

int main(){
    cin>>n>>T;
    
    int x,y;
    for(int i=0;i<n;++i){
        cin>>x>>y;
        v[i]=Triple(y,x,i+1);
    }

    sort(v,v+n);

    int l=0,r=n;
    int ans=-1;
    while(l!=r){
        bool pos=verify((l+r)/2);
        if(pos){
            ans=(l+r)/2;
            l=ans;
        }
        else{
            r=(l+r)/2;
        }
    }

    if(ans==-1)cout<<0<<endl<<0<<endl;
    else {cout<<ans<<endl<<ans<<endl;verify(ans,true);}

}
#include <iostream>
#include <cstring>
#include <map>

#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

int len;
string ans;

int main(){
    int t;
    cin>>t;
    string s;
    while(t--){
        ans="";
        cin>>s;
        cout<<s<<endl;
        len=s.length();
        cout<<len<<endl;
        if(len%2)REP(i,len-1)ans+='9';
        else{
            bool spec=true;
            for(int i=1;i<len-1;++i)if(s[i]!='0'){spec=false;break;}
            if(s[0]=='1' && s[len-1]<'1' && spec)REP(i,len-2)ans+='9';
            else{
                int l2=len/2;
                map<char,int> m;
                REP(i,l2)m[s[i]]++;
                bool fnd=false;
                for(int i=l2;i<len;++i){
                    char c;
                    REP(j,10)if(m['0'+j]){c='0'+j;m[c]--;break;}
                    if(c<s[i]){fnd=true;break;}
                    else if(c>s[i])break;
                }
                ans=s.substr(0,l2);
                if(!fnd){
                    int i=l2-1;
                    while(ans[i]=='0'){ans[i]='9';--i;}
                    ans[i]--;
                }
                map<char,int> am;
                REP(i,l2)am[ans[i]]++;
            }
        }
        cout<<ans<<endl;
    }
}
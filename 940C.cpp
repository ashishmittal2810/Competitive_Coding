#include <iostream>
#include <string>
#include <map>

using namespace std;

int n,k;
string s,t;
map<char,int> cnt;

int main(){
    cin>>n>>k;
    cin>>s;
    for(int i=0;i<n;++i)cnt[s[i]]=1;
    int from=min(k,n)-1;
    t=s.substr(0,min(k,n));
    if(k>n)for(int i=0;i<k-n;++i)t+='a';
    if(k<=n){
        for(;from>=0;--from){
            bool b=false;
            for(char c=s[from]+1;c<='z';++c){
                if(cnt[c]){
                    //cout<<"from "<<from<<endl;
                    t[from]=c;b=true;break;
                }
            }
            if(b)break;
        }
    }
    else{
        from=n;
    }
    for(int i=from+1;i<k;++i){
        for(char c='a';c<='z';++c){            
            if(cnt[c]){t[i]=c;break;}
        }
    }
    cout<<t<<endl;
}

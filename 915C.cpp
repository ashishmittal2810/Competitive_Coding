#include <iostream>
#include <cmath>

#define ll long long
#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

ll a,b;
int cnt[10];

int main(){
    cin>>a>>b;
    int noda=0,nodb=0;
    ll x;
    x=a;while(x!=0){x/=10;noda++;}
    x=b;while(x!=0){x/=10;nodb++;}
    ll cap;
    if(nodb>noda)cap=pow(10,noda)-1;
    else cap=b;
    x=a;while(x!=0){cnt[x%10]++;x/=10;}
    bool f9=false;
    ll ans=0;
    while(noda>0){
        --noda;
        if(f9){
            for(int i=9;i>=0;--i){
                if(cnt[i]){--cnt[i];ans+=(i*(ll)pow(10,noda));/*cout<<i<<" a "<<endl;*/break;}
            }
        }
        else{
            for(int i=((cap/(ll)pow(10,noda))%10);i>=0;--i){
                if(cnt[i]){
                    --cnt[i];
                    ans+=(i*(ll)pow(10,noda));
                    //cout<<i<<" ";
                    if(i<((cap/(ll)pow(10,noda))%10))f9=true;//cout<<"true "<<((cap/(ll)pow(10,noda))%10)<<" ";}
                    //cout<<endl;
                    break;
                }
            }
        }
    }
    cout<<ans<<endl;
}
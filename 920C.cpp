#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

const int maxN=2e+5;

int a[maxN]={};
string str;

int main(){
    int n;
    cin>>n;

    for(int i=0;i<n;++i)cin>>a[i];
    cin>>str;

    char prev='0',cur;
    int prev1=-1,maxE=-1;
    for(int i=0;i<n-1;++i){
        cur=str[i];
        if(prev=='0'){
            if(cur=='0'){
                if(a[i]!=i+1){cout<<"NO"<<endl;return 0;}
            }
            else{
                prev1=i;
                maxE=max(maxE,a[i]);
            }
        }
        else{
            if(cur=='0'){
                maxE=max(maxE,a[i]);
                if(maxE-1-prev1!=i-prev1){cout<<"NO"<<endl;return 0;}
            }
            else maxE=max(maxE,a[i]);
        }
        prev=cur;
    }
    cout<<"YES"<<endl;
}
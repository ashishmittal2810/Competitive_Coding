#include <iostream>
#include <vector>

#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

const int maxN=(5e+4)+5;

int n;
vector<int> primes;
vector<int> conn[maxN];
vector<int> centroidConn[maxN];
int siz[maxN];
int height[maxN]={};
int distFromAnc[maxN][20]={};
int centroidRoot=-1;
int a,b;
vector<bool> vertexInserted(false,maxN);

int getSize(int node,int p=-1){
    int& s=siz[node]=1;
    for(int c:conn[node]){
        if(c!=p)s+=getSize(c,node);
    }
    return s;
}

bool makeCentroidTree(int node=1,int s=n,int p=-1,int change=0,int h=0){
    siz[node]+=change;
    bool root=true;
    vertexInserted[1];
    int bc=-1;
    int si=0;
    int nsi;
    for(int c:conn[node]){
        nsi=siz[c]<=siz[node]?siz[c]:s-siz[node];
        if(!vertexInserted[c] && nsi>si){si=nsi;bc=c;}
        if(!vertexInserted[c] && nsi>(s/2)){root=false;break;}
    }
    if(root){
        if(p==-1)centroidRoot=node;
        else centroidConn[p].push_back(node);
        vertexInserted[node]=true;
        height[node]=h;
        for(int c:conn[node]){
            if(siz[c]<=siz[node])makeCentroidTree(c,siz[c],node,0,h+1);
            else makeCentroidTree(c,s-siz[node],node,change-siz[node],h+1);
        }
    }
    else{
        if(siz[bc]<=siz[node])makeCentroidTree(bc,s,p,0,h);
        else makeCentroidTree(bc,s,p,change,h);
    }
}

void dfsForHeight(int node,int h,int p=-1,int d=0){
    distFromAnc[node][height[node]-h]=d;
    for(int c:conn[node])
}

void findPathLengths(int node=centroidRoot,int h=0){
    dfsForHeight(centroidRoot,h);
    for(int c:centroidConn[node])dfsForHeight(c,h+1);
}

void findAllPrimes(){

}



int main(){
    cin>>n;

    REP(i,n-1){
        cin>>a>>b;
        conn[a].push_back(b);
        conn[b].push_back(a);
    }

    getSize(1);
    makeCentroidTree();
    findAllPrimes();
}
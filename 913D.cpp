#include <iostream>
#include <algorithm>

using namespace std;

struct Triple{
    int t,a,i;
    Triple(int _t=-1,int _a=-1,int _i=-1){
        t=_t;a=_a;i=_i;
    }
};

bool comp(Triple t1,Triple t2){
    return t1.t<t2.t;
}

const int maxN=(2e+5)+1;

int n,T;
Triple v[maxN];

bool verify(int a,bool print=false){
    int t=0;
    int d=0;
    for(int i=0;d<a && i<n;++i){
        if(v[i].a>=a){
            d++;
            t+=v[i].t;
            if(print)cout<<v[i].i<<" ";
        }
    }
    if(print)cout<<endl;
    if(t<=T && d==a)return true;
    else return false;
}

int main(){
    cin>>n>>T;
    
    int x,y;
    for(int i=0;i<n;++i){
        //cout<<i<<endl;
        cin>>x>>y;
        v[i]=Triple(y,x,i+1);
        //cout<<i<<endl;
    }
    //cout<<"end"<<endl;

    sort(v,v+n,comp);

    int l=0,r=n;
    int ans=-1;
    while(l<=r){
        //cout<<l<<" "<<r<<endl;
        bool pos=verify((l+r)/2);
        if(pos){
            ans=(l+r)/2;
            l=max(ans,l+1);
        }
        else{
            r=((l+r)/2)-1;
        }
    }

    if(ans==-1)cout<<0<<endl<<0<<endl;
    else {cout<<ans<<endl<<ans<<endl;verify(ans,true);}

}
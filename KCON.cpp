#include <iostream>
#include <algorithm>

#define REP(i,n) for(int i=0;i<n;++i)
#define ll long long

using namespace std;

const int maxN=1e+5;

int t,n,k;
ll a[maxN]={};

int main(){
    cin>>t;
    while(t--){
        ll s=0;
        cin>>n>>k;
        REP(i,n)cin>>a[i];
        ll csum=0,ssum=0,esum=0,msum=0;
        REP(i,n){csum+=a[i];ssum=max(csum,ssum);}
        for(int i=n-1;i>=0;--i){s+=a[i];esum=max(esum,s);}
        s=0;
        REP(i,n){
            s+=a[i];
            if(s<0)s=0;
            msum=max(msum,s);
        }

        ll ans=max(0ll,msum);
        if(k>=2){
            if(csum>0)ans=max(ans,ssum+esum+((k-2)*csum));
            else ans=max(ans,ssum+esum);
        }
        cout<<ans<<endl;

    }
}

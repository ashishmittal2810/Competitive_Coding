#include <iostream>
#include <string>
#include <vector>
#include <stack>

#define REP(i,n) for(int i=0;i<n;++i)
#define FOR(i,j,n) for(int i=j;i<n;++i)
#define RREP(i,n) for(int i=n-1;i>=0;--i)
#define RFOR(i,j,n) for(int i=n-1;i>=j;--i)

#define PIC pair<int,char>
#define PII pair<int,int>

using namespace std;

string s;
int n;
vector<PIC> cnt[2];
vector<int> v;
stack<PIC> c;
int ans=0;

int main(){
	cin>>s;
	n=s.length();
	int cur=0;

	char prev=s[0];
	int count=1;
	FOR(i,1,n){
		if(s[i]==prev)count++;
		else{
			cnt[0].push_back(PIC(count,prev));
			count=1;
			prev=s[i];
		}
	}
	cnt[0].push_back(PIC(count,prev));

	int ans=0;
	PIC p;
	int ns;
	while(cnt[cur].size()>1){
		//cout<<cur<<" "<<cnt[cur].size()<<endl;
		int siz=cnt[cur].size();
		REP(i,siz){
			p=cnt[cur][i];
			if(i==0 or i==(siz-1))ns=p.first-1;
			else ns=p.first-2;
			if(ns>0){
				if(cnt[1-cur].size()>0 and cnt[1-cur][cnt[1-cur].size()-1].second==p.second)cnt[1-cur][cnt[1-cur].size()-1].first+=ns;
				else cnt[1-cur].push_back(PIC(ns,p.second));
			}
		}
		cnt[cur].clear();
		ans++;
		cur=1-cur;
	}
	
	cout<<ans<<endl;
	return 0;
}

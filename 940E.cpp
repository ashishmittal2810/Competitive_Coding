#include <iostream>
#include <deque>

#define ll long long
#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

const int maxN=(1e+5)+5;
const ll inf=(1e+18);

void getMinInWindowSizeK(int* src,int k,int siz,int* dest){
    deque<int> mins;
    mins.push_back(siz-1);
    for(int i=siz-2;i>siz-k;--i){
        while(!mins.empty() && src[mins.back()]>src[i])mins.pop_back();
        mins.push_back(i);
    }
    for(int i=siz-k;i>=0;--i){
        while(!mins.empty() && src[mins.back()]>src[i])mins.pop_back();
        mins.push_back(i);
        while(!mins.empty() && mins.front()>=i+k)mins.pop_front();
        dest[i]=src[mins.front()];
    }
};

int n,c;
int a[maxN]={};
int mins[maxN]={};
ll dp[maxN][2];
ll sums[maxN]={};

ll solve(int k,int l){
    if(k==n)return 0;
    ll& s=dp[k][l];
    if(s==-1){
        if(l==1){
            if(k>n-c)s=inf;
            else s=sums[k]-mins[k]+min(solve(k+c,0),solve(k+c,1));
        }
        else s=a[k]+min(solve(k+1,0),solve(k+1,1));
    }
    return s;
}

int main(){
    cin>>n>>c;
    REP(i,n)cin>>a[i];
    getMinInWindowSizeK(a,c,n,mins);
    //REP(i,n+1-c)cout<<i<<":"<<mins[i]<<endl;
    ll sum=0;
    for(int i=n-1;i>n-c;--i)sum+=(ll)a[i];
    for(int i=n-c;i>=0;--i){sum-=(ll)a[i+c];sum+=a[i];sums[i]=sum;}
    REP(i,n)REP(j,2)dp[i][j]=-1;
    cout<<min(solve(0,0),solve(0,1))<<endl;
}
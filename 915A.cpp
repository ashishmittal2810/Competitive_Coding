#include <iostream>
#include <algorithm>

#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

int n,k;
int a[100];

int main(){
    cin>>n>>k;
    REP(i,n)cin>>a[i];

    int ans=200;
    REP(i,n)if(!(k%a[i]))ans=min(ans,k/a[i]);
    cout<<ans<<endl;
}
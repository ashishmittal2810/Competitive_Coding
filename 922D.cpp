#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#define REP(i,n) for(int i=0;i<n;++i)
#define P pair<int,int>
#define F first
#define S second
#define ll long long

using namespace std;

int n,s,h,ts,th;
string str;

vector<P> strs;

int c=0;

bool comp(P a,P b){
    //++c;
    //if(!(c%100000))cout<<c<<endl;
    //cout<<(a.F*b.S - a.S*b.F)<<endl;
    return ((ll)a.F*(ll)b.S - (ll)a.S*(ll)b.F)>0;
}

int main(){
    ts=0;th=0;
    ll ans=0;
    cin>>n;
    REP(i,n){
        cin>>str;
        s=0;h=0;
        REP(j,str.length()){
            if(str[j]=='s'){++s;ts++;}
            else {++h;th++;ans+=(ll)s;}
        }
        strs.push_back(P(s,h));
        //cout<<ans<<endl;
    }
    // n=100000;
    // REP(i,n){
    //     //cin>>str;
    //     s=0;h=0;
    //     if(i%4)str="h";
    //     else str="s";
    //     REP(j,str.length()){
    //         if(str[j]=='s'){++s;ts++;}
    //         else {++h;th++;ans+=(ll)s;}
    //     }
    //     strs.push_back(P(s,h));
    // }

    //cout<<"Check 1"<<endl;
    sort(strs.begin(),strs.end(),comp);
    //cout<<"Check 2"<<endl;
    int sd=0,hd=0;
    for(P p:strs){
        ans+=((ll)p.F*(ll)(th-(hd+p.S)));
        //cout<<ans<<endl;
        sd+=p.F;
        hd+=p.S;
        //cout<<p.F<<" "<<p.S<<endl;
    }

    cout<<ans<<endl;

}
#include <iostream>

#define REP(i,n) for(int i=0;i<n;++i)
#define FOR(i,j,n) for(int i=j;i<n;++i)

using namespace std;

static const int mod=(1e+9)+7;
static const int maxN=5e+3;

int n;
int ans=0;
char ch[maxN];
int dp[maxN][maxN];

int dps(int no,int ind){
	int &s=dp[no][ind];
	if(no==n)return 1;
	if(s==-1){
		s=0;
		if(ch[no]=='s'){
			if(no>0 and ch[no-1]=='f')s=dps(no+1,ind);
			else REP(i,ind+1){s+=dps(no+1,i);s%=mod;}
		}
		else{
			if(no>0 and ch[no-1]=='f')s=dps(no+1,ind+1);
			else FOR(i,1,ind+2){s+=dps(no+1,i);s%=mod;}
		}
	}
	return s;
}



int main(){
	cin>>n;
	REP(i,n)cin>>ch[i];
	REP(i,n)REP(j,n)dp[i][j]=-1;
	cout<<dps(0,0)<<endl;
}
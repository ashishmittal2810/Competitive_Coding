#include <iostream>
#include <cmath>
#include <vector>

#define ll long long

using namespace std;

ll n,m;

int main(){
    cin>>n>>m;
    
    while(n!=0 && m!=0 && n!=m){
       ll& a=m>n?m:n;
       ll& b=m<n?m:n;
       if(a>=2*b){
           ll d=((a-2*b)/(2*b)+1)*2*b;
           a-=d;
       }
       else break;
    }

    cout<<n<<" "<<m<<endl;
}
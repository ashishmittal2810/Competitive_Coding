#include <iostream>
#include <vector>
#include <algorithm>

#define REP(i,n) for(int i=0;i<n;++i)
#define FOR(i,j,n) for(int i=j;i<n+;++i)
#define P pair<int,int>//time,health

using namespace std;

struct update{
    int t,e,h;
    update(int _t=-1,int _e=-1,int _h=-1){
        t=_t;e=_e;h=_h;
    };
};

istream& operator >> (istream& in,update& u){
    in>>u.t>>u.e>>u.h;
    return in;
};

bool compUpdate(update u1,update u2){
    return u1.t<u2.t;
}

static const int maxN=1e+5;

int n,m,bounty,inc,dam,mh[maxN],sh[maxN],reg[maxN];
update updates[maxN];
P killTill[maxN];
vector<int> times;

int main(){
    cin>>n>>m;
    cin>>bounty>>inc>>dam;
    REP(i,n)cin>>mh[i]>>sh[i]>>reg[i];
    REP(i,m)cin>>updates[i];
    sort(updates,updates+m,compUpdate);

    int t;
    REP(i,n){
        if(sh[i]>dam)killTill[i]=P(-1,-1);
        else{
            t=(dam-sh[i])/reg[i];
            killTill[i]=P(t,reg[i]*t+sh[i]);
        }
    }

    int htt,hau;
    for(update u:updates){
        if(killTill[u.e].first!=-1 && killTill[u.e].first>u.t){
            htt=killTill[u.e].second-reg[u.e]*(killTill[u.e].first-u.t);//reg[u.e]*u.t+sh[u.e;
            hau=htt+u.h;
            if(hau>dam)killTill[u.e]=P(u.t-1,htt-reg[u.e]);
            else{
                t=(dam-hau)/reg[u.e];
                killTill[u.e]=P(t,hau+(t-u.t)*reg[u.e]);
            }
        }
    }

    REP(i,n)if(killTill[i].first!=-1)times.push_back(killTill[i].first);
    sort(times.begin(),times.end());

    int siz=times.size();

    int ans=0;
    REP(i,siz){
        ans=max(ans,(siz-i)*(bounty+(inc*times[i])));
        cout<<i<<" "<<times[i]<<" "<<ans<<" "<<((siz-i)*(bounty+(inc*times[i])))<<endl;
    }

    cout<<ans<<endl;

}
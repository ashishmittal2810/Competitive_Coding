#include <iostream>

using namespace std;

int t;
int a,b,c,d;

int main(){
    cin>>t;
    while(t--){
        cin>>a>>b>>c>>d;
        if(a==b && c==d || a==c && b==d || a==d && b==c)cout<<"YES"<<endl;
        else cout<<"NO"<<endl;
    }
}
#include <iostream>

#define ll long long

using namespace std;

ll n,k;
int main(){
    cin>>n>>k;

    if(k==1)cout<<n<<endl;
    else{
        ll ans=0;
        while(ans<n)ans=2*ans+1;
        cout<<ans<<endl;
    }
}
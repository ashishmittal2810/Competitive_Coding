#include <iostream>
#include <vector>
#include <queue>
#include <functional>

#define REP(i,n) for(int i=0;i<n;++i)
#define ll long long
#define P pair<ll,int>

using namespace std;

const int maxn=2e+5;

int n,m;
vector<P> conns[maxn];
ll a[maxn];
int visited[maxn];
priority_queue<P,vector<P>,greater<P>> q;
ll cost[maxn];

int main(){
    cin>>n>>m;
    int v,u;
    ll w;
    REP(i,m){
        cin>>v>>u>>w;
        v--;u--;
        conns[v].push_back(P(w,u));
        conns[u].push_back(P(w,v));
    }

    REP(i,n){cin>>a[i];q.push(P(a[i],i));}

    int done=0;
    P p;
    while(done!=n){
        p=q.top();q.pop();
        if(!visited[p.second]){
            visited[p.second]=1;
            done++;
            cost[p.second]=p.first;
            for(P i:conns[p.second]){
                if(!visited[i.second]){
                    q.push(P(p.first+2*i.first,i.second));
                }
            }
        }
    }

    REP(i,n)cout<<cost[i]<<" ";
    cout<<endl;

}
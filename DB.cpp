#include <iostream>
#include <algorithm>


#define ll long long
#define REP(i,n) for(int i=0;i<n;++i)

using namespace std;

const int M=(1e+9)+7;
int n;
ll dp[1001][1001]={};
ll e[1001][1001]={};
int b[1000];
int x;


ll dps(int nod,int no){
    if(nod==n){if(no==0)return 1ll;else return 0ll;}
    ll&s=dp[nod][no];
    if(s==-1){
        s=0;
        REP(i,min<int>(no+1,b[nod]+1))s=(s+dps(nod+1,no-i))%M;
    }

    return s;
}

int main(){
    cin>>n;
    REP(i,n)cin>>b[i];
    cin>>x;
    dp[n][0]=1;
    REP(i,x+1)e[n][i]=1;

    for(int i=n-1;i>=0;--i){
        for(int j=0;j<=x;++j){
            //cout<<"bef "<<i<<" "<<j<<" "<<dp[i][j]<<" "<<e[i][j]<<endl;
            dp[i][j]=e[i+1][j]-(((j-(min<int>(b[i],j)+1)))==-1?0:e[i+1][j-(min<int>(b[i],j)+1)]);
            e[i][j]=dp[i][j]+(j==0?0:e[i][j-1]);
            //cout<<"aft "<<i<<" "<<j<<" "<<dp[i][j]<<" "<<e[i][j]<<endl;
        }
    }
    cout<<dp[0][x]<<endl;

}
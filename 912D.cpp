#include <iostream>
#include <algorithm>
#include <queue>
#include <vector>
#include <map>
#include <iomanip>

#define REP(i,n) for(int i=0;i<n;++i)
#define P pair<int,int>

using namespace std;

struct Triple{
    int ar,a,b;
    Triple(int _ar=-1,int _a=-1,int _b=-1){
        ar=_ar;a=_a;b=_b;
    }
};

int n,m,r,k;
auto cmp = [](Triple l, Triple r) { return l.ar < r.ar;};
priority_queue<Triple, vector<Triple>, decltype(cmp)> q(cmp);
map<P,int> ma;
vector<int> v;

int main(){
    cin>>n>>m>>r>>k;
    int a=min<int>(n+1-r,r);
    int b=min<int>(m+1-r,r);
    
    int al=n-(2*a-2);
    int bl=m-(2*b-2);

    q.push(Triple(a*b,a,b));

    //cout<<"env "<<a<<" "<<b<<" "<<al<<" "<<bl<<endl;
    double ans=0;
    Triple t;
    int x,y;
    while(k){
        t=q.top();q.pop();
        if(!ma[P(t.a,t.b)]){
            if(t.a==a)x=al;else x=2;
            if(t.b==b)y=bl;else y=2;
            //cout<<"bef "<<t.ar<<" "<<t.a<<" "<<t.b<<" "<<x<<" "<<y<<" "<<ans<<" "<<k<<endl;
            ans+=(min<int>(k,x*y)*t.ar);
            k-=min<int>(k,x*y);
            if(t.a>1)q.push(Triple((t.a-1)*t.b,t.a-1,t.b));
            if(t.b>1)q.push(Triple((t.b-1)*t.a,t.a,t.b-1));
            ma[P(t.a,t.b)]=1;
            //cout<<"aft "<<t.ar<<" "<<t.a<<" "<<t.b<<" "<<x<<" "<<y<<" "<<ans<<" "<<k<<endl;
        }
    }
    //cout<<"ans "<<ans<<" "<<((n+1-r)*(m+1-r))<<endl;
    cout<<setprecision(15)<<(ans/(double)((n+1-r)*(m+1-r)))<<endl;

}
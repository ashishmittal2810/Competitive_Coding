#include <iostream>
#include <vector>
#include <queue>
#include <stack>

#define REP(i,n) for(int i=0;i<n;++i)
#define FOR(i,j,n) for(int i=j;i<n;++i)
#define RREP(i,n) for(int i=n-1;i>=0;--i)
#define RFOR(i,j,n) for(int i=n-1;i>=j;--i)

using namespace std;

const int maxN=1e+5;

int n,m;
int e[maxN+1]={};
int inDegree[maxN+1]={};
int level[maxN+1]={};
vector<int> pathTo[maxN+1];
vector<int> pathFrom[maxN+1];
vector<int> topSort;
queue<int> dfs;

int main(){
	cin>>n>>m;
	
	REP(i,n)cin>>e[i];
	
	int a,b;
	REP(i,m){
		cin>>a>>b;
		inDegree[a]++;
		pathTo[b].push_back(a);
		pathFrom[a].push_back(b);
	}

	REP(i,n){
		if(!inDegree[i]){
			inDegree[i]=1;
			pathTo[n].push_back(i);
			pathFrom[i].push_back(n);
		}
	}

	dfs.push(n);
	int t;
	while(!dfs.empty()){
		t=dfs.front();
		dfs.pop();
		inDegree[t]--;
		if(inDegree[t]==0)topSort.push_back(t);
		for(int p:pathTo[t])dfs.push(p);
	}

	int ans=0;

	for(int i:topSort){
		for(int p:pathFrom[i]){
			if(!e[i])level[i]=max(level[i],level[p]);
			else{
				if(e[p])level[i]=max(level[i],level[p]);
				else level[i]=max(level[i],level[p]+1);
			}
			//cout<<"stat "<<i<<" "<<p<<" "<<level[p]<<" "<<level[i]<<endl;
		}
		ans=max(ans,level[i]);
		//cout<<i<<" "<<level[i]<<" "<<ans<<endl;
	}

	cout<<ans<<endl;

}
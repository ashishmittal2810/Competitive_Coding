#include <iostream>

using namespace std;

int n;

int main(){
    cin>>n;
    int ans=0;
    int a,b,c;
    for(a=1;a<=n;++a){
        for(b=1;b<=a;++b){
            c=a^b;
            if(c<=b && (b+c)>a)ans++;
        }
    }
    cout<<ans<<endl;
}
